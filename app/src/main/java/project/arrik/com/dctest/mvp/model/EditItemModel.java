package project.arrik.com.dctest.mvp.model;

import android.os.Handler;
import android.text.TextUtils;

import project.arrik.com.dctest.model.Result;
import project.arrik.com.dctest.mvp.model.interfaces.IEditItemModel;
import project.arrik.com.dctest.mvp.view.interfaces.finish.IOnFinishedEditItem;

/**
 * Created by erdearik on 10/8/16.
 */

public class EditItemModel implements IEditItemModel {
    @Override
    public void setData(final Result result, final IOnFinishedEditItem iOnFinishedEditItem) {
        new Handler().postDelayed(new Runnable() {
            @Override public void run() {
                boolean error = false;
                if (result!=null){
                    iOnFinishedEditItem.onFinished(result);
                } else {
                    iOnFinishedEditItem.onFiled("Data null");
                }
            }
        }, 2000);
    }
}
