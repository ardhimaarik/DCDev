package project.arrik.com.dctest.mvp.view;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import project.arrik.com.dctest.R;
import project.arrik.com.dctest.base.BaseActivity;
import project.arrik.com.dctest.model.Result;
import project.arrik.com.dctest.mvp.model.interfaces.IEditItemModel;
import project.arrik.com.dctest.mvp.presenter.EditItemPresenter;
import project.arrik.com.dctest.mvp.presenter.interfaces.IEditItemPresenter;
import project.arrik.com.dctest.mvp.view.interfaces.view.IEditItemView;

public class DetailItemActivity extends BaseActivity implements IEditItemView {
    private EditText title, year, type;
    private ImageView img;
    public static String TAG = DetailItemActivity.class.getSimpleName();
    private Result result;
    private Result hasil;
    private IEditItemPresenter iEditItemPresenter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_item);

        title = (EditText) findViewById(R.id.et_title);
        year = (EditText) findViewById(R.id.et_year);
        type = (EditText) findViewById(R.id.et_type);

        img = (ImageView) findViewById(R.id.iv_img);

        img.setMaxHeight(img.getWidth());

        result = getIntent().getParcelableExtra(TAG);

        iEditItemPresenter = new EditItemPresenter(this);

        setDatas();
    }

    private void setDatas() {
        title.setText(result.getTitle());
        year.setText(result.getYear());
        type.setText(result.getType());

        Glide.with(this).load(result.getPoster()).into(img);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.edit,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == R.id.action_save){
            if(!TextUtils.isEmpty(title.getText().toString())
                    && !TextUtils.isEmpty(year.getText().toString())
                    && !TextUtils.isEmpty(type.getText().toString())){

                String titleS = title.getText().toString();
                String yearS = year.getText().toString();
                String typeS = type.getText().toString();

                Result hasil = new Result(titleS,yearS,typeS,result.getPoster(),result.getImdbID());
                iEditItemPresenter.setData(hasil);

            } else {
                showToast("Field cannot null!");
            }


        }
        return super.onOptionsItemSelected(item);
    }

    public static void start(Context context, Result result) {
        Intent starter = new Intent(context, DetailItemActivity.class);
        starter.putExtra(TAG,result);
        context.startActivity(starter);
    }

    @Override
    public void showProgress() {
        showProgressDialog("PLease Wait...");
    }

    @Override
    public void hideProgress() {
        dismissProgressDialog();
    }

    @Override
    public void setData(Result resultt) {
        this.result = resultt;
        Intent intent=new Intent();
        intent.putExtra("MESSAGE",resultt);
        setResult(2,intent);
        finish();//finishing activity
    }

    @Override
    public void setError(String s) {
        showToast(s);
    }
}
