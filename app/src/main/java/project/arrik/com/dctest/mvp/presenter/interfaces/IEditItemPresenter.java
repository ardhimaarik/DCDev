package project.arrik.com.dctest.mvp.presenter.interfaces;

import project.arrik.com.dctest.model.Result;

/**
 * Created by erdearik on 10/8/16.
 */

public interface IEditItemPresenter {
    void setData(Result result);
}
