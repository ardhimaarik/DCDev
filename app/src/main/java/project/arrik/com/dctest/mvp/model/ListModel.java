package project.arrik.com.dctest.mvp.model;

import java.util.List;

import project.arrik.com.dctest.model.Movie;
import project.arrik.com.dctest.model.Result;
import project.arrik.com.dctest.mvp.model.interfaces.IListModel;
import project.arrik.com.dctest.mvp.view.interfaces.finish.OnFinishedViewListener;
import project.arrik.com.dctest.network.ApiClient;
import project.arrik.com.dctest.network.ApiInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by erdearik on 10/8/16.
 */

public class ListModel implements IListModel {
    @Override
    public void getDataList(final OnFinishedViewListener onFinishedViewListener) {
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<Movie> call = apiService.getMovie("dragon");
        call.enqueue(new Callback<Movie>() {
            @Override
            public void onResponse(Call<Movie> call, Response<Movie> response) {
//                Log.e("asdasd", "sdsdf "+String.valueOf(response.body().getResult().get(0).getTitle()));
                List<Result> dataList = response.body().getResult();
                onFinishedViewListener.onFinished(dataList);
            }

            @Override
            public void onFailure(Call<Movie> call, Throwable t) {
                onFinishedViewListener.onError("Data Load Error");
            }
        });
    }
}
