package project.arrik.com.dctest.mvp.view;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v4.view.MenuItemCompat;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.List;

import project.arrik.com.dctest.base.BaseActivity;
import project.arrik.com.dctest.R;
import project.arrik.com.dctest.adapter.ItemAdapter;
import project.arrik.com.dctest.model.Result;
import project.arrik.com.dctest.mvp.presenter.ListPresenter;
import project.arrik.com.dctest.mvp.presenter.interfaces.IListPresenter;
import project.arrik.com.dctest.mvp.view.interfaces.view.IListView;

public class ListActivity extends BaseActivity implements IListView,SearchView.OnQueryTextListener{
    private RecyclerView recycleView;
    private LinearLayoutManager linearLayout;
    private ItemAdapter adapterGrid;
    private Context context;
    // Shared Preferences
    private SharedPreferences pref;
    // Editor for Shared preferences
    private SharedPreferences.Editor editor;
    List<Result> dataList;
    private IListPresenter iListPresenter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        context = this;
        pref = context.getSharedPreferences("DCTest", 0);
        editor = pref.edit();
        recycleView = (RecyclerView) findViewById(R.id.rv_item);

        iListPresenter = new ListPresenter(this);
        iListPresenter.getDataList();
    }

    private void setValue(List<Result> dataList) {
        if (pref.getString("data", null) != null) {
            String dataa = pref.getString("data", null);
            if (dataa.equals("grid")) {
                linearLayout = new GridLayoutManager(getApplicationContext(), 2);
                recycleView.setLayoutManager(linearLayout);
                adapterGrid = new ItemAdapter(this,dataList);
                recycleView.setAdapter(adapterGrid);
            } else {
                linearLayout = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
                recycleView.setLayoutManager(linearLayout);
                adapterGrid = new ItemAdapter(this,dataList);
                recycleView.setAdapter(adapterGrid);
            }

        } else {
            linearLayout = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
            recycleView.setLayoutManager(linearLayout);
            adapterGrid = new ItemAdapter(this,dataList);
            recycleView.setAdapter(adapterGrid);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.list,menu);
        final MenuItem item = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
        searchView.setOnQueryTextListener(this);

        MenuItemCompat.setOnActionExpandListener(item,
                new MenuItemCompat.OnActionExpandListener() {
                    @Override
                    public boolean onMenuItemActionCollapse(MenuItem item) {
                        // Do something when collapsed
                        adapterGrid.setFilter(dataList);
                        return true; // Return true to collapse action view
                    }

                    @Override
                    public boolean onMenuItemActionExpand(MenuItem item) {
                        // Do something when expanded
                        return true; // Return true to expand action view
                    }
                });

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == R.id.action_search){

        } else if(id == R.id.action_Grid){
            editor.putString("data","grid");
            editor.commit();
            if(dataList!=null) setValue(dataList);
        } else if(id == R.id.action_list){
            editor.putString("data","list");
            editor.commit();
            if(dataList!=null) setValue(dataList);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        final List<Result> filteredModelList = filter(dataList, query);
        adapterGrid.setFilter(filteredModelList);
        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }

    private List<Result> filter(List<Result> models, String query) {
        query = query.toLowerCase();

        final List<Result> filteredModelList = new ArrayList<>();
        for (Result model : models) {
            final String text = model.getTitle().toLowerCase();
            if (text.contains(query)) {
                filteredModelList.add(model);
            }
        }
        return filteredModelList;
    }

    @Override
    public void showProgress() {
        showProgressDialog("Please wait...");
    }

    @Override
    public void hideProgress() {
        dismissProgressDialog();
    }

    @Override
    public void getData(List<Result> dataList) {
        this.dataList = dataList;
        setValue(dataList);
    }

    @Override
    public void setError(String s) {
        showToast(s);
    }
}
