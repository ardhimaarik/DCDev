package project.arrik.com.dctest.mvp.view.interfaces.finish;

import project.arrik.com.dctest.model.Result;

/**
 * Created by erdearik on 10/8/16.
 */

public interface IOnFinishedEditItem {
    void onFinished(Result result);
    void onFiled(String s);
}
