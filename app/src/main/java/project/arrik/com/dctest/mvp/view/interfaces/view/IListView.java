package project.arrik.com.dctest.mvp.view.interfaces.view;

import java.util.List;

import project.arrik.com.dctest.model.Result;

/**
 * Created by erdearik on 10/8/16.
 */

public interface IListView {
    void showProgress();
    void hideProgress();
    void getData(List<Result> dataList);
    void setError(String s);
}
