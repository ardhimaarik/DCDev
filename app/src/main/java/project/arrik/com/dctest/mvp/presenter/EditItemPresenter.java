package project.arrik.com.dctest.mvp.presenter;

import project.arrik.com.dctest.model.Result;
import project.arrik.com.dctest.mvp.model.EditItemModel;
import project.arrik.com.dctest.mvp.model.interfaces.IEditItemModel;
import project.arrik.com.dctest.mvp.presenter.interfaces.IEditItemPresenter;
import project.arrik.com.dctest.mvp.view.DetailItemActivity;
import project.arrik.com.dctest.mvp.view.interfaces.finish.IOnFinishedEditItem;
import project.arrik.com.dctest.mvp.view.interfaces.view.IEditItemView;

/**
 * Created by erdearik on 10/8/16.
 */

public class EditItemPresenter implements IEditItemPresenter, IOnFinishedEditItem{
    private IEditItemView iEditItemView;
    private IEditItemModel iEditItemModel;

    public EditItemPresenter(DetailItemActivity detailItemActivity) {
        this.iEditItemView = detailItemActivity;
        this.iEditItemModel = new EditItemModel();
    }


    @Override
    public void setData(Result result) {
        iEditItemView.showProgress();
        iEditItemModel.setData(result,this);
    }

    @Override
    public void onFinished(Result result) {
        iEditItemView.hideProgress();
        iEditItemView.setData(result);
    }

    @Override
    public void onFiled(String s) {
        iEditItemView.hideProgress();
        iEditItemView.setError(s);
    }
}
