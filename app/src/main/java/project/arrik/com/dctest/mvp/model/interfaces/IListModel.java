package project.arrik.com.dctest.mvp.model.interfaces;

import project.arrik.com.dctest.mvp.view.interfaces.finish.OnFinishedViewListener;

/**
 * Created by erdearik on 10/8/16.
 */

public interface IListModel {
    void getDataList(OnFinishedViewListener onFinishedViewListener);
}
