package project.arrik.com.dctest.mvp.view.interfaces.finish;

import java.util.List;

import project.arrik.com.dctest.model.Result;

/**
 * Created by erdearik on 10/8/16.
 */

public interface OnFinishedViewListener {
    void onFinished(List<Result> results);
    void onError(String s);
}
