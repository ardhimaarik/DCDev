package project.arrik.com.dctest.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import project.arrik.com.dctest.R;
import project.arrik.com.dctest.model.Result;

/**
 * Created by erdearik on 10/7/16.
 */

public class ItemSearchAdapter extends RecyclerView.Adapter<ItemSearchAdapter.ViewHolder> {
    private Context context;
    private List<Result> list;

    public ItemSearchAdapter(Context context, List<Result> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_list, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Result data = list.get(position);
        holder.name.setText(data.getTitle());
        holder.year.setText(data.getYear());
        Glide.with(context).load(data.getPoster()).into(holder.iv);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

//    @Override
//    public Filter getFilter() {
//        Filter filter = new Filter() {
//
//            @SuppressWarnings("unchecked")
//            @Override
//            protected void publishResults(CharSequence constraint, FilterResults results) {
//
////                merchant = (LinkedHashMap<String, MerchantModel>) results.values;
//                notifyDataSetChanged();
//            }
//
//            @Override
//            protected FilterResults performFiltering(final CharSequence constraint) {
////                LinkedHashMap<String, MerchantModel> filteredResults;
////                if(constraint.length()>0) {
////                    filteredResults = new LinkedHashMap<>(Maps.filterEntries(app.merchant, new Predicate<Map.Entry<String,MerchantModel>>() {
////                        @Override
////                        public boolean apply(Map.Entry<String, MerchantModel> input) {
////                            return input.getValue().getName().toLowerCase().matches(".*" + constraint.toString().toLowerCase() + ".*");
////                        }}));
////                }else {
////                    filteredResults = app.merchant;
////                }
////
////                FilterResults results = new FilterResults();
////                results.values = filteredResults;
//
////                return results;
//            }
//        };
//        return filter;
//    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView name, year;
        public ImageView iv;

        public ViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.tv_name);
            year = (TextView) itemView.findViewById(R.id.tv_year);
            iv = (ImageView) itemView.findViewById(R.id.thumbnail);
        }
    }
}