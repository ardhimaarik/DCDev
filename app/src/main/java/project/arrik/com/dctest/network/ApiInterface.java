package project.arrik.com.dctest.network;

import project.arrik.com.dctest.model.Movie;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by erdearik on 10/7/16.
 */

public interface ApiInterface {
    @FormUrlEncoded
    @POST("dcc/test.php")
    Call<Movie> getMovie(@Field("passcode") String pass);
}
