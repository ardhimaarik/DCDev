package project.arrik.com.dctest.mvp.view;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import project.arrik.com.dctest.R;
import project.arrik.com.dctest.model.Result;

public class DetailViewActivity extends AppCompatActivity {
    private static String TAG = DetailViewActivity.class.getSimpleName();
    private Result result;
    private TextView title, year, type;
    private ImageView img;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_view);

        title = (TextView) findViewById(R.id.tv_title);
        year = (TextView) findViewById(R.id.tv_year);
        type = (TextView) findViewById(R.id.tv_type);

        img = (ImageView) findViewById(R.id.iv_img);

        img.setMaxHeight(img.getWidth());

        result = getIntent().getParcelableExtra(TAG);

        setData(result);


    }

    private void setData(Result result) {
        title.setText(result.getTitle());
        year.setText(result.getYear());
        type.setText(result.getType());
        Glide.with(this).load(result.getPoster()).into(img);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.view,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == R.id.action_edit){
            Intent intent=new Intent(DetailViewActivity.this,DetailItemActivity.class);
            intent.putExtra(DetailItemActivity.TAG,result);
            startActivityForResult(intent, 2);
//            DetailItemActivity.start(this,result);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode==2){
            result = data.getParcelableExtra("MESSAGE");
            setData(result);
        }
    }

    public static void start(Context context, Result result) {
        Intent starter = new Intent(context, DetailViewActivity.class);
        starter.putExtra(TAG, result);
        context.startActivity(starter);
    }
}
