package project.arrik.com.dctest.mvp.presenter.interfaces;

/**
 * Created by erdearik on 10/8/16.
 */

public interface IListPresenter {
    void getDataList();
}
