package project.arrik.com.dctest.mvp.presenter;

import java.util.List;

import project.arrik.com.dctest.model.Result;
import project.arrik.com.dctest.mvp.model.ListModel;
import project.arrik.com.dctest.mvp.model.interfaces.IListModel;
import project.arrik.com.dctest.mvp.presenter.interfaces.IListPresenter;
import project.arrik.com.dctest.mvp.view.interfaces.view.IListView;
import project.arrik.com.dctest.mvp.view.interfaces.finish.OnFinishedViewListener;

/**
 * Created by erdearik on 10/8/16.
 */

public class ListPresenter implements IListPresenter,OnFinishedViewListener {
    private IListView iListView;
    private IListModel iListModel;

    public ListPresenter(IListView iListView) {
        this.iListView = iListView;
        this.iListModel = new ListModel();
    }


    @Override
    public void getDataList() {
        iListView.showProgress();
        iListModel.getDataList(this);
    }

    @Override
    public void onFinished(List<Result> results) {
        iListView.hideProgress();
        iListView.getData(results);
    }

    @Override
    public void onError(String s) {
        iListView.hideProgress();
        iListView.setError(s);
    }
}
