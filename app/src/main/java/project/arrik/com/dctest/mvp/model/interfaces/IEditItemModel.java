package project.arrik.com.dctest.mvp.model.interfaces;

import project.arrik.com.dctest.model.Result;
import project.arrik.com.dctest.mvp.view.interfaces.finish.IOnFinishedEditItem;

/**
 * Created by erdearik on 10/8/16.
 */

public interface IEditItemModel {
    void setData(Result result, IOnFinishedEditItem iOnFinishedEditItem);
}
