package project.arrik.com.dctest;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import java.util.zip.Inflater;

public class MainActivity extends AppCompatActivity {

    private EditText item, radius;
    private Button btn;
    private ImageView img;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        item = (EditText) findViewById(R.id.et_item);
        radius = (EditText) findViewById(R.id.et_radius);

        btn = (Button) findViewById(R.id.btn);

        img = (ImageView) findViewById(R.id.iv);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int itemI, radiusI;
                String itemS = item.getText().toString();
                String radiusS = radius.getText().toString();
                if (!TextUtils.isEmpty(itemS) && !TextUtils.isEmpty(radiusS)) {
                    itemI = Integer.valueOf(itemS);
                    radiusI = Integer.valueOf(radiusS);

                    Paint paint = new Paint();
                    paint.setColor(Color.BLUE);
                    paint.setStyle(Paint.Style.STROKE);

                    Bitmap bitmap = Bitmap.createBitmap(500,500, Bitmap.Config.ARGB_8888);

                    Canvas canvas = new Canvas(bitmap);
                    canvas.drawCircle(bitmap.getWidth()/2, bitmap.getHeight()/2, radiusI, paint);

                    img.setImageBitmap(bitmap);

                    View[] tiles = new ImageView[itemI];
                    LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    for(int i = 0; i < itemI; i++){
                        ImageView image = (ImageView) inflater.inflate(R.layout.circle, null);
                        tiles[i] = image;
                        tiles[i].setId(i);
                        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(-1, 345);
                        params.leftMargin = 32*2*3;
                        params.topMargin = 34*2*3;
//                        layout.addView(tiles[i]);
                    }

                }
            }
        });
    }
}
