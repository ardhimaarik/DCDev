package project.arrik.com.dctest.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import project.arrik.com.dctest.mvp.view.DetailViewActivity;
import project.arrik.com.dctest.R;
import project.arrik.com.dctest.model.Result;

/**
 * Created by erdearik on 10/7/16.
 */

public class ItemAdapter extends RecyclerView.Adapter<ItemAdapter.ViewHolder> {
    private Context context;
    private List<Result> list;

    public ItemAdapter(Context context, List<Result> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_list, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Result data = list.get(position);
        holder.name.setText(data.getTitle());
        holder.year.setText(data.getYear());
        Glide.with(context).load(data.getPoster()).into(holder.iv);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DetailViewActivity.start(context, data);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void setFilter(List<Result> countryModels) {
        list = new ArrayList<>();
        list.addAll(countryModels);
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        public TextView name, year;
        public ImageView iv;
        public ViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.tv_name);
            year = (TextView) itemView.findViewById(R.id.tv_year);
            iv = (ImageView) itemView.findViewById(R.id.thumbnail);
        }
    }
}
